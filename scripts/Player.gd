extends KinematicBody

#const Bullet = preload("res://Bullet.tscn")
const GRAV = -9.8
const MAX_SPEED = 5
const JUMP_SPEED = 2.0
const ACCEL = 16.0
const DEACCEL = 8.0
const MAX_X_FOV = 89
const MAX_SLOPE_ANGLE = 40
const MOUSE_SENSITIVITY = 0.05
const DEATH_LINE_Y = -256
const NORMAL_HEIGHT = 1.5
const CROUCH_HEIGHT = NORMAL_HEIGHT / 2
const CROUCH_OFFSET = NORMAL_HEIGHT - CROUCH_HEIGHT
const CAMERA_HEIGHT = 0.375
const CROUCH_SPEED = 0.15

var vel = Vector3()
var spawn_transform
var totally_frozen = false
var frozen = 0
var shoot_timer = 0
var camera_y_offset = 0
var crouching = false
var xmove = []
var zmove = []
var since_on_floor = 0
var use_targets = []

func _ready():
	spawn_transform = transform
	
func do_crouch(delta):
	crouching = true
	camera_y_offset = min(camera_y_offset + CROUCH_OFFSET * (1/CROUCH_SPEED) * delta, CROUCH_HEIGHT)
	$CollisionShape.shape.height = CROUCH_HEIGHT
	$CollisionShape.translation.y = (NORMAL_HEIGHT - CROUCH_HEIGHT) / -2

func _physics_process(delta):
	if totally_frozen:
		return
	var dir = Vector3()
	var cam_xform = $Camera.get_global_transform()

	shoot_timer = max(shoot_timer - delta, 0)
	if frozen > 0:
		frozen = max(frozen - delta, 0)
	elif not Global.is_overlay_modal():
		if Input.is_action_just_pressed("move_forward"):
			zmove.append(-1)
		if Input.is_action_just_released("move_forward"):
			while zmove.has(-1):
				zmove.erase(-1)
		if Input.is_action_just_pressed("move_backward"):
			zmove.append(1)
		if Input.is_action_just_released("move_backward"):
			while zmove.has(1):
				zmove.erase(1)
		if Input.is_action_just_pressed("move_left"):
			xmove.append(-1)
		if Input.is_action_just_released("move_left"):
			while xmove.has(-1):
				xmove.erase(-1)
		if Input.is_action_just_pressed("move_right"):
			xmove.append(1)
		if Input.is_action_just_released("move_right"):
			while xmove.has(1):
				xmove.erase(1)
		if Input.is_action_just_pressed("use") and not use_targets.empty():
			use_targets[0].use(self)
		if Input.is_action_pressed("crouch"):
			do_crouch(delta)
		else:
			crouching = false
		if Input.is_action_just_pressed("shoot") and shoot_timer == 0:
			shoot_timer = 0.25
			#var bullet = Bullet.instance()
			#bullet.transform = $Camera.global_transform
			#get_parent().add_child(bullet)

	if !xmove.empty():
		dir += cam_xform.basis.x.normalized() * xmove[0]
	if !zmove.empty():
		dir += cam_xform.basis.z.normalized() * zmove[0]

	if is_on_floor():
		spawn_transform = transform
		since_on_floor = 0
	else:
		since_on_floor += delta
		
	var snap = Vector3(0, -1, 0)
	if since_on_floor < 0.05 and not frozen and not Global.is_overlay_modal():
		if Input.is_action_pressed("move_jump"):
			snap = Vector3(0, 0, 0)
			vel.y = JUMP_SPEED

	if crouching == false:
		if $CollisionShape.shape.height == CROUCH_HEIGHT:
			$CollisionShape.shape.height = NORMAL_HEIGHT
			$CollisionShape.translation.y = 0
			var collide = move_and_collide(Vector3(0, 1.0/64.0, 0), true, true)
			if collide == null:
				camera_y_offset = max(camera_y_offset - CROUCH_OFFSET * (1/CROUCH_SPEED) * delta, 0)
			else:
				do_crouch(1)
		else:
			camera_y_offset = max(camera_y_offset - CROUCH_OFFSET * (1/CROUCH_SPEED) * delta, 0)

	$Camera.translation.y = CAMERA_HEIGHT - camera_y_offset
	
	dir.y = 0
	dir = dir.normalized()

	var grav = GRAV
	vel.y += delta*grav
	if vel.y < 0:
		vel.y *= 1.0 + 1.0/16.0

	var hvel = vel
	hvel.y = 0

	var target = dir
	target *= MAX_SPEED

	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL

	hvel = hvel.linear_interpolate(target, accel*delta)
	vel.x = hvel.x
	vel.z = hvel.z
	vel = move_and_slide_with_snap(vel, snap, Vector3(0, 1, 0), true, true, 3)

	if translation.y < DEATH_LINE_Y:
		transform = spawn_transform
		translation = translation.snapped(Vector3(2, 2, 2)) + Vector3(-1, 2, -1)
		print("respawning at " + str(translation))
		frozen = 1
		vel = Vector3()
		xmove = []
		zmove = []

func _input(event):
	if totally_frozen:
		return
	if event is InputEventMouseMotion && Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		$Camera.rotate_x(deg2rad(event.relative.y * MOUSE_SENSITIVITY * -1))
		self.rotate_y(deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1))

		var camera_rot = $Camera.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, -MAX_X_FOV, MAX_X_FOV)
		$Camera.rotation_degrees = camera_rot
