extends Node

const World = preload("res://scenes/World.tscn")

func _on_GenerateButton_pressed():
	var world = World.instance()
	world.map_size = $MainMenu/MapSize.value
	get_tree().change_scene_to(world)
	get_tree().root.add_child(world)
	queue_free()

func _on_QuitButton_pressed():
	get_tree().quit()