extends Spatial

const SCALE = 1.0
const Y_SCALE = SCALE * 8.0
const Y_SCALE_HILLS = SCALE * 32.0
const Y_SCALE_WATER = SCALE * 16.0
const CHUNKSIZE = 8.0
const REALCHUNKSIZE = CHUNKSIZE * SCALE
const VIEWCHUNKS = 2.0

export (NodePath) var viewpoint_path = null
export (Material) var material = null

onready var noise = OpenSimplexNoise.new()
onready var noise2 = OpenSimplexNoise.new()
onready var noise3 = OpenSimplexNoise.new()
var viewpoint: Spatial = null
var active_chunks : Dictionary = {}
var work_routine : GDScriptFunctionState = null
	
func _ready():
	viewpoint = get_node(viewpoint_path)
	noise.seed = randi()
	noise.octaves = 4
	noise.period = 256.0
	noise.persistence = 0.9
	noise2.seed = randi()
	noise2.octaves = 2
	noise2.period = 256.0
	noise2.persistence = 0.4
	noise3.seed = randi()
	noise3.octaves = 1
	noise3.period = 512.0
	noise3.persistence = 0.2
	
	for pos in _get_visible_chunks():
		var result = _create_chunk(pos)
		while typeof(result) != TYPE_DICTIONARY:
			result = result.resume()
		active_chunks[result["pos"]] = result["body"]
		
func height_at(pos):
	return _vec3_at({}, pos.x / SCALE, pos.z / SCALE)

func _vec3_at(cache, x, z):
	var pos = Vector2(x, z)
	if not cache.has(pos):
		cache[pos] = noise.get_noise_2d(x, z) * Y_SCALE
		var m = noise2.get_noise_2d(x, z)
		var n = noise3.get_noise_2d(x, z)
		n = range_lerp(n, -1, 1, 1, 4)
		if m >= -0.5:
			var f = range_lerp(m, -0.5, 1, 0, 1)
			cache[pos] += pow(f, n) * Y_SCALE_HILLS
		elif m < -0.5:
			var f = abs(range_lerp(m, -1, -0.5, -1, 0))
			cache[pos] += pow(f, n) * Y_SCALE_WATER
	return Vector3(x * SCALE, cache[pos], z * SCALE)

func _normal_at(cache, p):
	var x = p.x / SCALE
	var z = p.z / SCALE
	var a = _vec3_at(cache, x + 1, z)
	var b = _vec3_at(cache, x, z + 1)
	var c = _vec3_at(cache, x - 1, z)
	var d = _vec3_at(cache, x, z - 1)
	return -((a - p).cross(b - p) + (c - p).cross(d - p)).normalized()

func _create_chunk(pos : Vector3):
	var cache = {}
	
	var verts = PoolVector3Array()
	var normals = PoolVector3Array()
	var tangents = PoolRealArray()
	for z in range(pos.z, pos.z + CHUNKSIZE):
		for x in range(pos.x, pos.x + CHUNKSIZE):
			var a = _vec3_at(cache, x, z) - pos
			var b = _vec3_at(cache, x + 1, z) - pos
			var c = _vec3_at(cache, x + 1, z + 1) - pos
			var d = _vec3_at(cache, x, z + 1) - pos
			# Flat shading commented out here
			"""
			var ne = -(b - a).cross(d - a).normalized()
			var nf = (c - d).cross(b - d).normalized()
			var te = (b - a).normalized()
			var tf = (c - d).normalized()
			"""
			var na = _normal_at(cache, a + pos)
			var nb = _normal_at(cache, b + pos)
			var nc = _normal_at(cache, c + pos)
			var nd = _normal_at(cache, d + pos)
			var ta = na.cross(b - a).normalized()
			var tb = nb.cross(_vec3_at(cache, x + 2, z) - b).normalized()
			var tc = nc.cross(_vec3_at(cache, x + 2, z + 1) - c).normalized()
			var td = nd.cross(c - d).normalized()
			
			verts.push_back(a)
			verts.push_back(b)
			verts.push_back(d)
			verts.push_back(d)
			verts.push_back(b)
			verts.push_back(c)
			"""
			normals.push_back(ne)
			normals.push_back(ne)
			normals.push_back(ne)
			normals.push_back(nf)
			normals.push_back(nf)
			normals.push_back(nf)
			tangents.push_back(te.x); tangents.push_back(te.y); tangents.push_back(te.z); tangents.push_back(1)
			tangents.push_back(te.x); tangents.push_back(te.y); tangents.push_back(te.z); tangents.push_back(1)
			tangents.push_back(te.x); tangents.push_back(te.y); tangents.push_back(te.z); tangents.push_back(1)
			tangents.push_back(tf.x); tangents.push_back(tf.y); tangents.push_back(tf.z); tangents.push_back(1)
			tangents.push_back(tf.x); tangents.push_back(tf.y); tangents.push_back(tf.z); tangents.push_back(1)
			tangents.push_back(tf.x); tangents.push_back(tf.y); tangents.push_back(tf.z); tangents.push_back(1)
			"""
			normals.push_back(na)
			normals.push_back(nb)
			normals.push_back(nd)
			normals.push_back(nd)
			normals.push_back(nb)
			normals.push_back(nc)
			tangents.push_back(ta.x); tangents.push_back(ta.y); tangents.push_back(ta.z); tangents.push_back(1)
			tangents.push_back(tb.x); tangents.push_back(tb.y); tangents.push_back(tb.z); tangents.push_back(1)
			tangents.push_back(td.x); tangents.push_back(td.y); tangents.push_back(td.z); tangents.push_back(1)
			tangents.push_back(td.x); tangents.push_back(td.y); tangents.push_back(td.z); tangents.push_back(1)
			tangents.push_back(tb.x); tangents.push_back(tb.y); tangents.push_back(tb.z); tangents.push_back(1)
			tangents.push_back(tc.x); tangents.push_back(tc.y); tangents.push_back(tc.z); tangents.push_back(1)
		
		yield()
	
	var mesh = ArrayMesh.new()
	var arrays = []
	arrays.resize(ArrayMesh.ARRAY_MAX)
	arrays[ArrayMesh.ARRAY_VERTEX] = verts
	arrays[ArrayMesh.ARRAY_NORMAL] = normals
	arrays[ArrayMesh.ARRAY_TANGENT] = tangents
	mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
	mesh.surface_set_material(0, material)
	
	var body = CSGMesh.new()
	body.translation = pos
	body.use_collision = true
	body.collision_mask = 0
	body.operation = CSGShape.OPERATION_UNION
	body.mesh = mesh
	body.name = "Chunk"+str(pos)
	
	yield()
	add_child(body)
	return {"pos": pos, "body": body}
	
func _get_visible_chunks():
	var cam_pos = viewpoint.get_global_transform().origin / SCALE
	var sta = cam_pos - Vector3(CHUNKSIZE, 0, CHUNKSIZE) * VIEWCHUNKS
	var end = cam_pos + Vector3(CHUNKSIZE, 0, CHUNKSIZE) * (VIEWCHUNKS + 1.0)
	
	var active = []
	for z in range(sta.z, end.z, CHUNKSIZE):
		for x in range(sta.x, end.x, CHUNKSIZE):
			var pos = Vector3(stepify(x, CHUNKSIZE), 0, stepify(z, CHUNKSIZE))
			active.append(pos)
	return active
	
func _process(delta):
	var inactive = active_chunks.keys()
	
	if work_routine != null:
		var result = work_routine.resume()
		if typeof(result) != TYPE_DICTIONARY:
			work_routine = result
		else:
			active_chunks[result["pos"]] = result["body"]
			work_routine = null
	
	for pos in _get_visible_chunks():
		if work_routine == null and not active_chunks.has(pos):
			work_routine = _create_chunk(pos)
		inactive.erase(pos)
	
	for pos in inactive:
		var body = active_chunks[pos]
		remove_child(body)
		body.queue_free()
		active_chunks.erase(pos)