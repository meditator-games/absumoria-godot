extends Spatial

const Elevator = preload("res://actors/Elevator.tscn")
const BuildingBlocks = preload("res://scenes/BuildingBlocks.meshlib")
var timer = 0
var map_size = 15
var buildings = []

const YRotations = [0, 16, 10, 22]

enum CollisionLayers {
	CL_BUILDINGS,
	CL_GLASS,
	CL_GROUND,
	CL_DESTRUCTIBLES,
	CL_PLAYER,
	CL_NPCS
}

enum GridDirections {
	X_UP,
	X_DN,
	Y_UP,
	Y_DN,
	Z_UP,
	Z_DN,
	W_X_UP,
	W_X_DN,
	W_Z_UP,
	W_Z_DN
}

enum Directions {
	SOUTH,
	EAST,
	NORTH,
	WEST
}

const GridNames = ["X+", "X-", "Y+", "Y-", "Z+", "Z-", "WX+", "WX-", "WZ+", "WZ-"]

enum RoadCellTypes {
	C_ROAD,
	C_ROAD_SIDE,
	C_ROAD_CORNER,
	C_ROAD_CORNER_INNER,
	C_ROAD_LINE,
	C_ROAD_LINE_DOUBLE,
	C_ROAD_XWALK,
	C_SIDEWALK,
	C_SIDEWALK_SIDE,
	C_SIDEWALK_CORNER
}

enum BuildingCellTypes {
	C_WALL_BOT,
	C_WALL_MID,
	C_WALL_TOP,
	C_WALL_BOT_L_CORNER,
	C_WALL_MID_L_CORNER,
	C_WALL_TOP_L_CORNER,
	C_WALL_BOT_R_CORNER,
	C_WALL_MID_R_CORNER,
	C_WALL_TOP_R_CORNER,
	
	C_WINDOW_BOT,
	C_WINDOW_MID,
	C_WINDOW_TOP,
	C_WINDOW_BOT_L_CORNER,
	C_WINDOW_MID_L_CORNER,
	C_WINDOW_TOP_L_CORNER,
	C_WINDOW_BOT_R_CORNER,
	C_WINDOW_MID_R_CORNER,
	C_WINDOW_TOP_R_CORNER,
	
	C_FLOOR,
	C_FLOOR_SIDE,
	C_FLOOR_CORNER,
	C_CEILING,
	C_CEILING_SIDE,
	C_CEILING_CORNER,
	C_WINDOWPANE
}

func _enter_tree():
	randomize()
	
const NEIGHBOR_DELTAS = [Vector2(0, 1), Vector2(1, 0), Vector2(0, -1), Vector2(-1, 0)]
const NEIGHBOR_DELTAS_DIAG = [Vector2(1, 1), Vector2(-1, 1), Vector2(-1, -1), Vector2(1, -1)]

func _ready():
	Global.cur_player = $Player
	$RoadGrid.clear()
	get_viewport().debug_draw = Viewport.DEBUG_DRAW_WIREFRAME
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	var building_regions = [{"type": "building", "cells": [Vector2(0, 0)]}]
	var active_regions = [building_regions[0]]
	var cells = {Vector2(0, 0): building_regions[0]}
	while cells.size() < map_size:
		var cur_regions = active_regions.duplicate()
		while not cur_regions.empty():
			# shuffle the list
			var region_index = randi() % cur_regions.size()
			var region = cur_regions[region_index]
			cur_regions.remove(region_index)
			# check for empty neighbors
			var grow_targets = []
			var spawn_targets = []
			for growspot in region["cells"]:
				for i in range(NEIGHBOR_DELTAS.size()):
					var neighbor = NEIGHBOR_DELTAS[i] + growspot
					if not cells.has(neighbor):
						grow_targets.append(neighbor)
						for j in range(NEIGHBOR_DELTAS.size()):
							var neighbor2 = NEIGHBOR_DELTAS[i] + neighbor
							if not cells.has(neighbor2):
								spawn_targets.append(neighbor2)
			if spawn_targets.empty() and grow_targets.empty():
				active_regions.erase(region)
				continue
			if not spawn_targets.empty() and randi() % 5 == 0:
				var new_region = {"type": "building", "cells": [spawn_targets[randi() % spawn_targets.size()]]}
				cells[new_region["cells"][0]] = new_region
				active_regions.append(new_region)
			elif not grow_targets.empty() and randi() % region["cells"].size() == 0:
				region["cells"].append(grow_targets[randi() % grow_targets.size()])
				cells[region["cells"][-1]] = region
	#spawn roads
	for growspot in cells.keys():
		for i in range(NEIGHBOR_DELTAS.size()):
			var neighbor = NEIGHBOR_DELTAS[i] + growspot
			if not cells.has(neighbor):
				cells[neighbor] = {"type": "road"}
				
	# make sure you can turn corners around buildings
	var potential_intersections = {}
	for growspot in cells.keys():
		if cells[growspot]["type"] == "road":
			for i in range(NEIGHBOR_DELTAS.size()):
				var neighbor = NEIGHBOR_DELTAS[i] + growspot
				if not cells.has(neighbor):
					if potential_intersections.has(neighbor):
						potential_intersections[neighbor]["roads"] += 1
					else:
						potential_intersections[neighbor] = {"roads": 1, "buildings": 0}
		elif cells[growspot]["type"] == "building":
			for i in range(NEIGHBOR_DELTAS_DIAG.size()):
				var neighbor = NEIGHBOR_DELTAS_DIAG[i] + growspot
				if not cells.has(neighbor):
					if potential_intersections.has(neighbor):
						potential_intersections[neighbor]["buildings"] += 1
					else:
						potential_intersections[neighbor] = {"roads": 0, "buildings": 1}
	for roadspot in potential_intersections.keys():
		var inter = potential_intersections[roadspot]
		if inter["roads"] >= 2 and inter["buildings"] >= 1:
			cells[roadspot] = {"type": "road"}
	
	var cell_min = Vector2()
	var cell_max = Vector2()
	var cellkeys = cells.keys()
	for pos in cellkeys:
		cell_min.x = min(pos.x, cell_min.x)
		cell_max.x = max(pos.x, cell_max.x)
		cell_min.y = min(pos.y, cell_min.y)
		cell_max.y = max(pos.y, cell_max.y)
		
	var bwidths = {}
	var bdepths = {}
	var boffsets_x = {int(cell_min.x): 0}
	var boffsets_z = {int(cell_min.y): 0}
	for x in range(cell_min.x, cell_max.x + 1):
		var w = randi() % 8 + 8 + 4
		bwidths[int(x)] = w
		boffsets_x[int(x) + 1] = boffsets_x[int(x)] + w
	for z in range(cell_min.y, cell_max.y + 1): # y in Vector2 = z in Vector3
		var d = randi() % 10 + 9 + 4
		bdepths[int(z)] = d
		boffsets_z[int(z) + 1] = boffsets_z[int(z)] + d
		
	# TODO - make an array of row/column sizes
	$Player.totally_frozen = true
	var startcell = cellkeys[randi() % cellkeys.size()]
	$Player.translation.x = boffsets_x[int(startcell.x)] * $RoadGrid.cell_size.x + 1
	$Player.translation.z = boffsets_z[int(startcell.y)] * $RoadGrid.cell_size.z + 1
	var cam_pos = Vector3(0, 256, 0)
	cam_pos.x = (boffsets_x[int(cell_min.x)] + boffsets_x[int(cell_max.x)]) / 2.0 * $RoadGrid.cell_size.x
	cam_pos.z = (boffsets_z[int(cell_min.y)] + boffsets_z[int(cell_max.y)]) / 2.0 * $RoadGrid.cell_size.z
	$Player/Camera.global_transform = Transform(Basis(Vector3(1, 0, 0), PI * -0.5), cam_pos)
	yield(get_tree(), "idle_frame")
	var intersections = {}
	for pos in cells.keys():
		var bwidth = bwidths[int(pos.x)]
		var bdepth = bdepths[int(pos.y)]
		var region = cells[pos]
		var neighbors = []
		for i in range(NEIGHBOR_DELTAS.size()):
			var neighbor = NEIGHBOR_DELTAS[i] + pos
			if cells.has(neighbor) and cells[neighbor]["type"] == region["type"]:
				neighbors.append(i)
		var sta = Vector3(boffsets_x[int(pos.x)], 0, boffsets_z[int(pos.y)])
		var under_sta = sta - Vector3(0, 1, 0)
		var base_size = Vector3(bwidth, 1, bdepth)
		if region["type"] == "road":
			var result = _spawn_road(under_sta, base_size, neighbors)
			if result == "intersection":
				intersections[pos] = [under_sta, base_size, neighbors]
			else:
				yield(get_tree(), "idle_frame")
		else:
			var height = randi() % 3 + 2
			var floors = [height]
			var f = randf()
			var floor_count = floor(f * f * 16) + 1
			for f in range(floor_count):
				var fheight = 2
				floors.append(fheight)
				height += fheight
			_spawn_sidewalk(under_sta, base_size, neighbors)
			var corner = sta + Vector3(2, 0, 2)
			#corner.y = $Terrain.height_at(corner + base_size / 2.0).y + 1
			var doors = []
			var potential_doors = []
			for x in range(base_size.x):
				potential_doors.append(corner + Vector3(x, 0, 0))
				potential_doors.append(corner + Vector3(x, 0, base_size.z - 1))
			for z in range(1, base_size.z - 1):
				potential_doors.append(corner + Vector3(0, 0, z))
				potential_doors.append(corner + Vector3(base_size.x - 1, 0, z))
			for i in range(randi() % 3 + 1):
				var index = randi() % potential_doors.size()
				doors.append(potential_doors[index])
				potential_doors.remove(index)
			var size = Vector3(bwidth - 4, height, bdepth - 4)
			var building = _spawn_building(corner, size, doors, floors)
			_create_elevator(building)
			buildings.append(building)
			yield(get_tree(), "idle_frame")
	
	# do intersections afterwards to avoid new roads drawn over xwalks
	for pos in intersections.keys():
		var inter = intersections[pos]
		var no_xwalks = []
		for i in range(NEIGHBOR_DELTAS.size()):
			var neighbor = NEIGHBOR_DELTAS[i] + pos
			if intersections.has(neighbor):
				no_xwalks.append(i)
		_spawn_intersection(inter[0], inter[1], inter[2], no_xwalks)
		yield(get_tree(), "idle_frame")
	# zoom back in to player viewpoint
	$Tween.interpolate_property($Player/Camera, "transform", null, Transform(), 2.0, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	$Player.totally_frozen = false
	
	#terrain off for now
	#$Player.translation.y = $Terrain.height_at(Vector3()).y + 1

func _spawn_road(sta, size, open):
	var end = sta + size - Vector3(1, 1, 1)
	var grid = $RoadGrid
	if open.size() == 1:
		# reverse yrot[] 3 and 1 here if they're wrong
		if open[0] == SOUTH:
			grid.set_cell_item(sta.x, end.y, end.z, C_ROAD_CORNER, YRotations[3])
			grid.set_cell_item(end.x, end.y, end.z, C_ROAD_CORNER, YRotations[0])
			for x in range(sta.x + 1, end.x):
				grid.set_cell_item(x, end.y, end.z, C_ROAD_SIDE, YRotations[0])
			_spawn_straight_road(sta, VERTICAL, size.z - 1, size.x)
		elif open[0] == EAST:
			grid.set_cell_item(sta.x, end.y, sta.z, C_ROAD_CORNER, YRotations[2])
			grid.set_cell_item(sta.x, end.y, end.z, C_ROAD_CORNER, YRotations[1])
			for z in range(sta.z + 1, end.z):
				grid.set_cell_item(sta.x, end.y, z, C_ROAD_SIDE, YRotations[1])
			_spawn_straight_road(sta + Vector3(1, 0, 0), HORIZONTAL, size.x - 1, size.z)
		elif open[0] == NORTH:
			grid.set_cell_item(sta.x, end.y, sta.z, C_ROAD_CORNER, YRotations[2])
			grid.set_cell_item(end.x, end.y, sta.z, C_ROAD_CORNER, YRotations[1])
			for x in range(sta.x + 1, end.x):
				grid.set_cell_item(x, end.y, sta.z, C_ROAD_SIDE, YRotations[2])
			_spawn_straight_road(sta + Vector3(0, 0, 1), VERTICAL, size.z - 1, size.x)
		elif open[0] == WEST:
			grid.set_cell_item(end.x, end.y, sta.z, C_ROAD_CORNER, YRotations[3])
			grid.set_cell_item(end.x, end.y, end.z, C_ROAD_CORNER, YRotations[0])
			for z in range(sta.z + 1, end.z):
				grid.set_cell_item(end.x, end.y, z, C_ROAD_SIDE, YRotations[3])
			_spawn_straight_road(sta, HORIZONTAL, size.x - 1, size.z)
		return "straight"
	elif open.size() == 2:
		# array should be sorted
		if open[0] == SOUTH and open[1] == NORTH:
			_spawn_straight_road(sta, VERTICAL, size.z, size.x)
			return "straight"
		elif open[0] == EAST and open[1] == WEST:
			_spawn_straight_road(sta, HORIZONTAL, size.x, size.z)
			return "straight"
		else:
			return "intersection"
	else: 
		return "intersection"

func _create_elevator(building):
	var lower = building["lower"]
	var size = building["size"]
	var elev_pos = lower + Vector3(1 + randi() % int(size.x - 4), 0, 1 + randi() % int(size.z - 4))
	var dir = randi() % 4
	for y in range(lower.y, lower.y + size.y):
		for i in range(2):
			building[Y_DN].set_cell_item(elev_pos.x + i, y, elev_pos.z, -1)
			building[Y_DN].set_cell_item(elev_pos.x + i, y, elev_pos.z + 1, -1)
			if dir != 0:
				building[Z_UP].set_cell_item(elev_pos.x + i, y, elev_pos.z - 1, C_WALL_MID, YRotations[0])
			if dir != 1:
				building[X_UP].set_cell_item(elev_pos.x - 1, y, elev_pos.z + i, C_WALL_MID, YRotations[1])
			if dir != 2:
				building[Z_DN].set_cell_item(elev_pos.x + i, y, elev_pos.z + 2, C_WALL_MID, YRotations[2])
			if dir != 3:
				building[X_DN].set_cell_item(elev_pos.x + 2, y, elev_pos.z + i, C_WALL_MID, YRotations[3])
	var cell_size = building[Y_DN].cell_size
	var elev = Elevator.instance()
	elev.translation = elev_pos * 2.0 + Vector3(cell_size.x, 1.5, cell_size.z)
	elev.rotation_degrees.y = (dir + 1) * 90
	elev.translation += elev.transform.basis.x * 0.5
	var elev_target = elev.translation
	for fh in building["floors"]:
		elev.floors.append(elev_target)
		elev_target.y += fh * cell_size.y
	add_child(elev)
	building["elevators"].append(elev)
	

func _spawn_sidewalk(sta: Vector3, size: Vector3, open_edges: Array = []):
	if size.x < 2 or size.y < 1 or size.z < 2:
		return
	var grid = $RoadGrid
	var end = sta + size - Vector3(1, 1, 1)
	grid.set_cell_item(sta.x, end.y, sta.z, C_SIDEWALK_CORNER, YRotations[2])
	grid.set_cell_item(end.x, end.y, sta.z, C_SIDEWALK_CORNER, YRotations[1])
	grid.set_cell_item(end.x, end.y, end.z, C_SIDEWALK_CORNER, YRotations[0])
	grid.set_cell_item(sta.x, end.y, end.z, C_SIDEWALK_CORNER, YRotations[3])
	for z in range(sta.z + (0 if open_edges.has(SOUTH) else 1), end.z + (1 if open_edges.has(NORTH) else 0)):
		grid.set_cell_item(sta.x, end.y, z, C_SIDEWALK_SIDE, YRotations[3])
		grid.set_cell_item(end.x, end.y, z, C_SIDEWALK_SIDE, YRotations[1])
	for x in range(sta.x + (0 if open_edges.has(WEST) else 1), end.x + (1 if open_edges.has(EAST) else 0)):
		grid.set_cell_item(x, end.y, sta.z, C_SIDEWALK_SIDE, YRotations[2])
		grid.set_cell_item(x, end.y, end.z, C_SIDEWALK_SIDE, YRotations[0])
	for z in range(sta.z + 1, end.z):
		for x in range(sta.x + 1, end.x):
			grid.set_cell_item(x, end.y, z, C_SIDEWALK)
	# bottom is all road blocks
	for y in range(sta.y, end.y):
		for z in range(sta.z, sta.z + size.z):
			for x in range(sta.x, sta.x + size.x):
				grid.set_cell_item(x, y, z, C_ROAD)
				
func _fill_road_grid(sta: Vector3, size: Vector3, tile: int = -1, orientation: int = 0):
	for z in range(sta.z, sta.z + size.z):
		for y in range(sta.y, sta.y + size.y):
			for x in range(sta.x, sta.x + size.x):
				$RoadGrid.set_cell_item(x, y, z, tile, orientation)

func _spawn_intersection(sta: Vector3, size: Vector3, open: Array, no_xwalk: Array):
	var grid = $RoadGrid
	_fill_road_grid(sta, size, C_ROAD)
	var end = sta + size - Vector3(1, 1, 1)
	for x in range(sta.x, sta.x + size.x):
		if open.has(SOUTH) and not no_xwalk.has(SOUTH):
			grid.set_cell_item(x, end.y, end.z + 1, C_ROAD)
			grid.set_cell_item(x, end.y, end.z + 2, C_ROAD_XWALK, YRotations[1])
		if open.has(NORTH) and not no_xwalk.has(NORTH):
			grid.set_cell_item(x, end.y, sta.z - 2, C_ROAD_XWALK, YRotations[1])
			grid.set_cell_item(x, end.y, sta.z - 1, C_ROAD)
	for z in range(sta.z, sta.z + size.z):
		if open.has(WEST) and not no_xwalk.has(WEST):
			grid.set_cell_item(sta.x - 2, end.y, z, C_ROAD_XWALK, YRotations[0])
			grid.set_cell_item(sta.x - 1, end.y, z, C_ROAD)
		if open.has(EAST) and not no_xwalk.has(EAST):
			grid.set_cell_item(end.x + 1, end.y, z, C_ROAD)
			grid.set_cell_item(end.x + 2, end.y, z, C_ROAD_XWALK, YRotations[0])
	

func _spawn_straight_road(sta: Vector3, orientation: int, length: int, width: int):
	var grid = $RoadGrid
	var left_lanes = []
	var right_lanes = []
	var left_gutter = 0
	var right_gutter = 0
	if width < 2:
		left_gutter = width
	elif width < 7:
		left_lanes.append(width - 2)
	else:
		var width_left = width - 1
		var on_left = true
		while width_left >= 3:
			if on_left:
				left_lanes.append(2)
			else:
				right_lanes.append(2)
			on_left = not on_left
			width_left -=3
		if width_left == 2:
			right_gutter += 1
		if width_left >= 1:
			left_gutter += 1
	if orientation == HORIZONTAL:
		for x in range(sta.x, sta.x + length):
			var z = sta.z
			for i in range(left_gutter):
				grid.set_cell_item(x, sta.y, z, C_ROAD)
				z += 1
			if left_lanes.size() > 0 or right_lanes.size() > 0:
				grid.set_cell_item(x, sta.y, z, C_ROAD_SIDE, YRotations[0])
				z += 1
			for i in range(left_lanes.size()):
				for w in range(left_lanes[i]):
					grid.set_cell_item(x, sta.y, z, C_ROAD)
					z += 1
				if i < left_lanes.size() - 1:
					grid.set_cell_item(x, sta.y, z, (C_ROAD_LINE if x % 2 == 0 else C_ROAD), YRotations[1])
					z += 1
			if left_lanes.size() > 0 and right_lanes.size() > 0:
				grid.set_cell_item(x, sta.y, z, C_ROAD_LINE_DOUBLE, YRotations[1])
				z += 1
			for i in range(right_lanes.size()):
				for w in range(right_lanes[i]):
					grid.set_cell_item(x, sta.y, z, C_ROAD)
					z += 1
				if i < right_lanes.size() - 1:
					grid.set_cell_item(x, sta.y, z, (C_ROAD_LINE if x % 2 == 0 else C_ROAD), YRotations[1])
					z += 1
			if left_lanes.size() > 0 or right_lanes.size() > 0:
				grid.set_cell_item(x, sta.y, z, C_ROAD_SIDE, YRotations[2])
				z += 1
			for i in range(right_gutter):
				grid.set_cell_item(x, sta.y, z, C_ROAD)
				z += 1
	elif orientation == VERTICAL:
		for z in range(sta.z, sta.z + length):
			var x = sta.x
			for i in range(left_gutter):
				grid.set_cell_item(x, sta.y, z, C_ROAD)
				x += 1
			if left_lanes.size() > 0 or right_lanes.size() > 0:
				grid.set_cell_item(x, sta.y, z, C_ROAD_SIDE, YRotations[1])
				x += 1
			for i in range(left_lanes.size()):
				for w in range(left_lanes[i]):
					grid.set_cell_item(x, sta.y, z, C_ROAD)
					x += 1
				if i < left_lanes.size() - 1:
					grid.set_cell_item(x, sta.y, z, (C_ROAD_LINE if z % 2 == 0 else C_ROAD), YRotations[2])
					x += 1
			if left_lanes.size() > 0 and right_lanes.size() > 0:
				grid.set_cell_item(x, sta.y, z, C_ROAD_LINE_DOUBLE, YRotations[2])
				x += 1
			for i in range(right_lanes.size()):
				for w in range(right_lanes[i]):
					grid.set_cell_item(x, sta.y, z, C_ROAD)
					x += 1
				if i < right_lanes.size() - 1:
					grid.set_cell_item(x, sta.y, z, (C_ROAD_LINE if z % 2 == 0 else C_ROAD), YRotations[2])
					x += 1
			if left_lanes.size() > 0 or right_lanes.size() > 0:
				grid.set_cell_item(x, sta.y, z, C_ROAD_SIDE, YRotations[3])
				x += 1
			for i in range(right_gutter):
				grid.set_cell_item(x, sta.y, z, C_ROAD)
				x += 1

func _physics_process(delta):
	timer += delta
	$Clouds1.translation.x = $Player.translation.x
	$Clouds1.translation.z = $Player.translation.z
	$Clouds2.translation.x = $Player.translation.x
	$Clouds2.translation.z = $Player.translation.z
	var cloud_uv_offset = Vector3(-$Clouds1.translation.x/65535.0, -$Clouds1.translation.z/65535.0, 0.0)
	var cloud1_offset = timer * (1.0/128.0)
	var cloud2_offset = timer * (1.0/192.0)
	$Clouds1.get_surface_material(0).set_shader_param("uv1_offset",
		cloud_uv_offset + Vector3(cloud1_offset, cloud1_offset, 0))
	$Clouds2.get_surface_material(0).set_shader_param("uv1_offset",
		cloud_uv_offset + Vector3(-cloud2_offset, -cloud2_offset * 0.25, 0))

func _spawn_building(sta, size, doors, floors):
	if size.x < 2 or size.y < 2 or size.z < 2:
		return null
	
	var end = sta + size - Vector3(1, 1, 1)
	
	var building = {
		"root": Spatial.new(),
		"lower": sta,
		"size": size,
		"center": sta + size / 2.0,
		"floors": floors,
		"elevators": []
	}
	building["root"].name = "BuildingRoot"
	for i in range(10):
		var grid = GridMap.new()
		grid.cell_size = Vector3(2, 2, 2)
		grid.mesh_library = BuildingBlocks
		building["root"].add_child(grid)
		grid.name = "Grid" + GridNames[i]
		building[i] = grid
		#grid.visible = false
		grid.collision_mask = 0
		if i < W_X_UP:
			grid.set_collision_layer_bit(CL_BUILDINGS, true)
		else:
			grid.set_collision_layer_bit(CL_GLASS, true)
	
	var yf = sta.y
	for yo in floors:
		building[Y_DN  ].set_cell_item(sta.x, yf, sta.z, C_FLOOR_CORNER, YRotations[2])
		building[Y_DN  ].set_cell_item(end.x, yf, sta.z, C_FLOOR_CORNER, YRotations[1])
		building[Y_DN  ].set_cell_item(end.x, yf, end.z, C_FLOOR_CORNER, YRotations[0])
		building[Y_DN  ].set_cell_item(sta.x, yf, end.z, C_FLOOR_CORNER, YRotations[3])
		for x in range(1, size.x - 1):
			building[Y_DN  ].set_cell_item(sta.x + x, yf, sta.z, C_FLOOR_SIDE, YRotations[2])
			building[Y_DN  ].set_cell_item(sta.x + x, yf, end.z, C_FLOOR_SIDE, YRotations[0])
			for z in range(1, size.z - 1):
				building[Y_DN].set_cell_item(sta.x + x, yf, sta.z + z, C_FLOOR)
		for z in range(1, size.z - 1):
			building[Y_DN  ].set_cell_item(sta.x, yf, sta.z + z, C_FLOOR_SIDE, YRotations[3])
			building[Y_DN  ].set_cell_item(end.x, yf, sta.z + z, C_FLOOR_SIDE, YRotations[1])
		yf += yo
		# bot corners
	
	if not doors.has(sta):
		building[Z_DN  ].set_cell_item(sta.x, sta.y, sta.z, C_WINDOW_BOT_L_CORNER, YRotations[3])
		building[X_DN  ].set_cell_item(sta.x, sta.y, sta.z, C_WINDOW_BOT_R_CORNER, YRotations[2])
		building[W_Z_DN].set_cell_item(sta.x, sta.y, sta.z, C_WINDOWPANE, YRotations[3])
		building[W_X_DN].set_cell_item(sta.x, sta.y, sta.z, C_WINDOWPANE, YRotations[2])
	
	if not doors.has(Vector3(end.x, sta.y, sta.z)):
		building[Z_DN  ].set_cell_item(end.x, sta.y, sta.z, C_WINDOW_BOT_L_CORNER, YRotations[2])
		building[X_UP  ].set_cell_item(end.x, sta.y, sta.z, C_WINDOW_BOT_R_CORNER, YRotations[1])
		building[W_Z_DN].set_cell_item(end.x, sta.y, sta.z, C_WINDOWPANE, YRotations[2])
		building[W_X_UP].set_cell_item(end.x, sta.y, sta.z, C_WINDOWPANE, YRotations[1])
	
	if not doors.has(Vector3(end.x, sta.y, end.z)):
		building[Z_UP  ].set_cell_item(end.x, sta.y, end.z, C_WINDOW_BOT_L_CORNER, YRotations[1])
		building[X_UP  ].set_cell_item(end.x, sta.y, end.z, C_WINDOW_BOT_R_CORNER, YRotations[0])
		building[W_Z_UP].set_cell_item(end.x, sta.y, end.z, C_WINDOWPANE, YRotations[1])
		building[W_X_UP].set_cell_item(end.x, sta.y, end.z, C_WINDOWPANE, YRotations[0])
	
	if not doors.has(Vector3(sta.x, sta.y, end.z)):
		building[Z_UP  ].set_cell_item(sta.x, sta.y, end.z, C_WINDOW_BOT_L_CORNER, YRotations[0])
		building[X_DN  ].set_cell_item(sta.x, sta.y, end.z, C_WINDOW_BOT_R_CORNER, YRotations[3])
		building[W_Z_UP].set_cell_item(sta.x, sta.y, end.z, C_WINDOWPANE, YRotations[0])
		building[W_X_DN].set_cell_item(sta.x, sta.y, end.z, C_WINDOWPANE, YRotations[3])
	
	# top corners
		
	building[Z_DN  ].set_cell_item(sta.x, end.y, sta.z, C_WINDOW_TOP_L_CORNER, YRotations[3])
	building[X_DN  ].set_cell_item(sta.x, end.y, sta.z, C_WINDOW_TOP_R_CORNER, YRotations[2])
	building[W_Z_DN].set_cell_item(sta.x, end.y, sta.z, C_WINDOWPANE, YRotations[3])
	building[W_X_DN].set_cell_item(sta.x, end.y, sta.z, C_WINDOWPANE, YRotations[2])
	building[Y_UP  ].set_cell_item(sta.x, end.y, sta.z, C_CEILING_CORNER, YRotations[2])
	
	building[Z_DN  ].set_cell_item(end.x, end.y, sta.z, C_WINDOW_TOP_L_CORNER, YRotations[2])
	building[X_UP  ].set_cell_item(end.x, end.y, sta.z, C_WINDOW_TOP_R_CORNER, YRotations[1])
	building[W_Z_DN].set_cell_item(end.x, end.y, sta.z, C_WINDOWPANE, YRotations[2])
	building[W_X_UP].set_cell_item(end.x, end.y, sta.z, C_WINDOWPANE, YRotations[1])
	building[Y_UP  ].set_cell_item(end.x, end.y, sta.z, C_CEILING_CORNER, YRotations[1])
	
	building[Z_UP  ].set_cell_item(end.x, end.y, end.z, C_WINDOW_TOP_L_CORNER, YRotations[1])
	building[X_UP  ].set_cell_item(end.x, end.y, end.z, C_WINDOW_TOP_R_CORNER, YRotations[0])
	building[W_Z_UP].set_cell_item(end.x, end.y, end.z, C_WINDOWPANE, YRotations[1])
	building[W_X_UP].set_cell_item(end.x, end.y, end.z, C_WINDOWPANE, YRotations[0])
	building[Y_UP  ].set_cell_item(end.x, end.y, end.z, C_CEILING_CORNER, YRotations[0])
	
	building[Z_UP  ].set_cell_item(sta.x, end.y, end.z, C_WINDOW_TOP_L_CORNER, YRotations[0])
	building[X_DN  ].set_cell_item(sta.x, end.y, end.z, C_WINDOW_TOP_R_CORNER, YRotations[3])
	building[W_Z_UP].set_cell_item(sta.x, end.y, end.z, C_WINDOWPANE, YRotations[0])
	building[W_X_DN].set_cell_item(sta.x, end.y, end.z, C_WINDOWPANE, YRotations[3])
	building[Y_UP  ].set_cell_item(sta.x, end.y, end.z, C_CEILING_CORNER, YRotations[3])

	for y in range(1, size.y - 1):
		# corner columns
		building[Z_DN  ].set_cell_item(sta.x, sta.y + y, sta.z, C_WINDOW_MID_L_CORNER, YRotations[3])
		building[X_DN  ].set_cell_item(sta.x, sta.y + y, sta.z, C_WINDOW_MID_R_CORNER, YRotations[2])
		building[W_Z_DN].set_cell_item(sta.x, sta.y + y, sta.z, C_WINDOWPANE, YRotations[3])
		building[W_X_DN].set_cell_item(sta.x, sta.y + y, sta.z, C_WINDOWPANE, YRotations[2])
			
		building[Z_DN  ].set_cell_item(end.x, sta.y + y, sta.z, C_WINDOW_MID_L_CORNER, YRotations[2])
		building[X_UP  ].set_cell_item(end.x, sta.y + y, sta.z, C_WINDOW_MID_R_CORNER, YRotations[1])
		building[W_Z_DN].set_cell_item(end.x, sta.y + y, sta.z, C_WINDOWPANE, YRotations[2])
		building[W_X_UP].set_cell_item(end.x, sta.y + y, sta.z, C_WINDOWPANE, YRotations[1])
		
		building[Z_UP  ].set_cell_item(end.x, sta.y + y, end.z, C_WINDOW_MID_L_CORNER, YRotations[1])
		building[X_UP  ].set_cell_item(end.x, sta.y + y, end.z, C_WINDOW_MID_R_CORNER, YRotations[0])
		building[W_Z_UP].set_cell_item(end.x, sta.y + y, end.z, C_WINDOWPANE, YRotations[1])
		building[W_X_UP].set_cell_item(end.x, sta.y + y, end.z, C_WINDOWPANE, YRotations[0])
		
		building[Z_UP  ].set_cell_item(sta.x, sta.y + y, end.z, C_WINDOW_MID_L_CORNER, YRotations[0])
		building[X_DN  ].set_cell_item(sta.x, sta.y + y, end.z, C_WINDOW_MID_R_CORNER, YRotations[3])
		building[W_Z_UP].set_cell_item(sta.x, sta.y + y, end.z, C_WINDOWPANE, YRotations[0])
		building[W_X_DN].set_cell_item(sta.x, sta.y + y, end.z, C_WINDOWPANE, YRotations[3])
	
	for x in range(1, size.x - 1):
		# floor z-sides
		if not doors.has(Vector3(sta.x + x, sta.y, sta.z)):
			building[Z_DN  ].set_cell_item(sta.x + x, sta.y, sta.z, C_WINDOW_BOT, YRotations[2])
			building[W_Z_DN].set_cell_item(sta.x + x, sta.y, sta.z, C_WINDOWPANE, YRotations[2])
		if not doors.has(Vector3(sta.x + x, sta.y, end.z)):
			building[Z_UP  ].set_cell_item(sta.x + x, sta.y, end.z, C_WINDOW_BOT, YRotations[0])
			building[W_Z_UP].set_cell_item(sta.x + x, sta.y, end.z, C_WINDOWPANE, YRotations[0])
		
		# ceiling z-sides
		building[Z_DN  ].set_cell_item(sta.x + x, end.y, sta.z, C_WINDOW_TOP, YRotations[2])
		building[W_Z_DN].set_cell_item(sta.x + x, end.y, sta.z, C_WINDOWPANE, YRotations[2])
		building[Y_UP  ].set_cell_item(sta.x + x, end.y, sta.z, C_CEILING_SIDE, YRotations[2])
		building[Z_UP  ].set_cell_item(sta.x + x, end.y, end.z, C_WINDOW_TOP, YRotations[0])
		building[W_Z_UP].set_cell_item(sta.x + x, end.y, end.z, C_WINDOWPANE, YRotations[0])
		building[Y_UP  ].set_cell_item(sta.x + x, end.y, end.z, C_CEILING_SIDE, YRotations[0])
		
		for y in range(1, size.y - 1):
			# z-wall columns
			building[Z_DN  ].set_cell_item(sta.x + x, sta.y + y, sta.z, C_WINDOW_MID, YRotations[2])
			building[W_Z_DN].set_cell_item(sta.x + x, sta.y + y, sta.z, C_WINDOWPANE, YRotations[2])
			building[Z_UP  ].set_cell_item(sta.x + x, sta.y + y, end.z, C_WINDOW_MID, YRotations[0])
			building[W_Z_UP].set_cell_item(sta.x + x, sta.y + y, end.z, C_WINDOWPANE, YRotations[0])
		
		for z in range(1, size.z - 1):
			#top
			building[Y_UP].set_cell_item(sta.x + x, end.y, sta.z + z, C_CEILING, YRotations[0])
			
	for z in range(1, size.z - 1):
		# floor x-sides
		if not doors.has(Vector3(sta.x, sta.y, sta.z + z)):
			building[X_DN  ].set_cell_item(sta.x, sta.y, sta.z + z, C_WINDOW_BOT, YRotations[3])
			building[W_X_DN].set_cell_item(sta.x, sta.y, sta.z + z, C_WINDOWPANE, YRotations[3])
		if not doors.has(Vector3(end.x, sta.y, sta.z + z)):
			building[X_UP  ].set_cell_item(end.x, sta.y, sta.z + z, C_WINDOW_BOT, YRotations[1])
			building[W_X_UP].set_cell_item(end.x, sta.y, sta.z + z, C_WINDOWPANE, YRotations[1])
		
		# ceiling x-sides
		building[X_DN  ].set_cell_item(sta.x, end.y, sta.z + z, C_WINDOW_TOP, YRotations[3])
		building[W_X_DN].set_cell_item(sta.x, end.y, sta.z + z, C_WINDOWPANE, YRotations[3])
		building[Y_UP  ].set_cell_item(sta.x, end.y, sta.z + z, C_CEILING_SIDE, YRotations[3])
		building[X_UP  ].set_cell_item(end.x, end.y, sta.z + z, C_WINDOW_TOP, YRotations[1])
		building[W_X_UP].set_cell_item(end.x, end.y, sta.z + z, C_WINDOWPANE, YRotations[1])
		building[Y_UP  ].set_cell_item(end.x, end.y, sta.z + z, C_CEILING_SIDE, YRotations[1])
		
		for y in range(1, size.y - 1):
			#x-wall columns
			building[X_DN  ].set_cell_item(sta.x, sta.y + y, sta.z + z, C_WINDOW_MID, YRotations[3])
			building[W_X_DN].set_cell_item(sta.x, sta.y + y, sta.z + z, C_WINDOWPANE, YRotations[3])
			building[X_UP  ].set_cell_item(end.x, sta.y + y, sta.z + z, C_WINDOW_MID, YRotations[1])
			building[W_X_UP].set_cell_item(end.x, sta.y + y, sta.z + z, C_WINDOWPANE, YRotations[1])
	
	add_child(building["root"])
	#var mask = CSGBox.new()
	#mask.translation = sta + size / 2.0
	#mask.width = size.x + 1
	#mask.height = size.y + 1
	#mask.depth = size.z + 1
	#mask.operation = CSGShape.OPERATION_SUBTRACTION
	#mask.material = $Terrain.material
	#$Terrain.add_child(mask)
	
	return building
