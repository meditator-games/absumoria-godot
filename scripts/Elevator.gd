extends KinematicBody

const ElevatorDialog = preload("res://ui/ElevatorDialog.tscn")

const MOVE_SPEED = 2

var floors = []
var current_floor = 0
var use_overlay = null
onready var target_pos = translation

func use(body):
	if floors.size() <= 1:
		return
	var overlay = ElevatorDialog.instance()
	overlay.connect("submit", self, "_move_to_floor")
	overlay.prompt(floors, current_floor)
	Global.push_overlay(overlay, true)
	
func _physics_process(delta):
	var diff = target_pos - translation
	var dl = diff.length_squared()
	if dl > 0:
		dl = sqrt(dl)
		if dl >= MOVE_SPEED * delta:
			var dir = diff / dl * MOVE_SPEED * delta
			translation += dir
		else:
			translation = target_pos

func _move_to_floor(floor_index):
	target_pos = floors[floor_index]
	current_floor = floor_index

func _on_InteriorArea_body_entered(body):
	if floors.size() <= 1:
		return
	if body == Global.cur_player:
		body.use_targets.push_front(self)
		var label = Label.new()
		label.text = "Press " + Global.get_bind_str("use") + " to Use Elevator"
		label.set_anchors_preset(Control.PRESET_CENTER)
		label.margin_top += OS.window_size.y  * 0.25
		Global.push_overlay(label)
		use_overlay = label

func _on_InteriorArea_body_exited(body):
	if body == Global.cur_player:
		body.use_targets.erase(self)
		Global.remove_overlay(use_overlay)
		use_overlay = null
