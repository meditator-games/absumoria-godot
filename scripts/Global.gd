extends Node

var cur_player: Spatial = null
var overlay_stack = []

func push_overlay(control, modal=false):
	if not overlay_stack.empty():
		overlay_stack[-1]["control"].visible = false
	overlay_stack.append({"control": control, "modal": modal})
	get_tree().root.add_child(control)
			
func remove_overlay(control):
	for entry in overlay_stack:
		if entry["control"] == control:
			overlay_stack.erase(entry)
			control.queue_free()
			break
	if not overlay_stack.empty():
		overlay_stack[-1]["control"].visible = true

func is_overlay_modal():
	if not overlay_stack.empty():
		return overlay_stack[-1]["modal"]
	return false

func current_overlay():
	if not overlay_stack.empty():
		return overlay_stack[-1]["control"]
	return null

func get_bind_str(action):
	var key = action
	var binds = InputMap.get_action_list(action)
	if not binds.empty():
		key = binds[0].as_text()
		for i in range(1, binds.size()):
			key += " or " + binds[i].as_text()
	return key