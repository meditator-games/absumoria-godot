extends Control

signal submit(floor_index)

onready var _count_label = $ColorRect/FloorCount
onready var _entry = $ColorRect/HBoxContainer/FloorEntry
var _floors = 0
var _current = 0

func _ready():
	for i in range(_floors):
		if i != 0:
			_count_label.text += "  "
		_count_label.text += str(i + 1)
	
	_entry.max_value = _floors
	_entry.value = _current + 1
	modulate = Color(1, 1, 1, 0)
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	$Tween.interpolate_property(self, "modulate", null, Color(1, 1, 1, 1), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	_entry.grab_focus()
	
func _close():
	release_focus()
	mouse_filter = Control.MOUSE_FILTER_IGNORE
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	$Tween.interpolate_property(self, "modulate", null, Color(1, 1, 1, 0), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	$Tween.connect("tween_completed", self, "_clear_self")

func _clear_self(obj, key):
	Global.remove_overlay(self)

func prompt(floors: Array, current: int):
	_floors = floors.size()
	_current = current

func _process(delta):
	if Global.current_overlay() == self:
		if Input.is_action_just_pressed("ui_up"):
			_entry.value += 1
			_entry.grab_focus()
		elif Input.is_action_just_pressed("ui_down"):
			_entry.value -= 1
			_entry.grab_focus()
		elif Input.is_action_just_pressed("ui_accept"):
			_on_Submit_pressed()
		elif Input.is_action_just_pressed("ui_cancel"):
			_close()

func _on_Submit_pressed():
	emit_signal("submit", _entry.value - 1)
	_close()
