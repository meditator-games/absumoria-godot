extends ColorRect

onready var ref_probe: ReflectionProbe = get_node("/root/Root/Player/ReflectionProbe")
onready var gi_probe: GIProbe = get_node("/root/Root/Player/GIProbe")
onready var env: Environment = get_node("/root/Root/Player/Camera").environment

func _ready():
	visible = false
	modulate = Color(1, 1, 1, 0)
	toggle_menu(true)
	
	$GridContainer/Reflections.pressed = ref_probe.visible and gi_probe.visible and env.ss_reflections_enabled
	#$GridContainer/Reflections.pressed = env.ss_reflections_enabled
	$GridContainer/MSAA.add_item("Disabled", Viewport.MSAA_DISABLED)
	$GridContainer/MSAA.add_item("2x", Viewport.MSAA_2X)
	$GridContainer/MSAA.add_item("4x", Viewport.MSAA_4X)
	$GridContainer/MSAA.add_item("8x", Viewport.MSAA_8X)
	$GridContainer/MSAA.add_item("16x", Viewport.MSAA_16X)
	$GridContainer/MSAA.select(get_viewport().msaa)
	
func toggle_menu(enabled):
	get_tree().paused = not enabled
	if enabled:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		$Tween.interpolate_property(self, "modulate", null, Color(1, 1, 1, 0), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$Tween.start()
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		$Tween.interpolate_property(self, "modulate", null, Color(1, 1, 1, 1), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$Tween.start()

func _process(delta):
	if not Global.is_overlay_modal() and Input.is_action_just_pressed("ui_cancel"):
		toggle_menu(get_tree().paused)
	
	if modulate.a >= 1.0:
		mouse_filter = Control.MOUSE_FILTER_STOP
	else:
		mouse_filter = Control.MOUSE_FILTER_IGNORE
	visible = modulate.a > 0

func _on_Reflections_toggled(pressed):
	ref_probe.visible = pressed
	gi_probe.visible = pressed
	env.ss_reflections_enabled = pressed

func _on_MSAA_item_selected(id):
	get_viewport().msaa = id

func _on_ReturnButton_pressed():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	get_tree().paused = false
	get_tree().root.get_node("Root").queue_free()
	get_tree().change_scene("res://ui/Title.tscn")
	
func _on_QuitButton_pressed():
	get_tree().quit()
