extends KinematicBody

const Hole = preload("res://Hole.gd")

const SPEED = 10

func _ready():
	pass

func _physics_process(delta):
	var collide = move_and_collide(-transform.basis.z * delta * SPEED, true, true)
	if collide != null:
		queue_free()
		var hole = Hole.new()
		hole.translation = translation
		get_node("../Platforms").add_child(hole)
	else:
		move_and_collide(-transform.basis.z * delta * SPEED)