extends CSGSphere

const Mat = preload("res://BPanel.material")

var MAX_TIMER = 2

var timer = MAX_TIMER
var init_radius = null

func _ready():
	radius = 2
	radial_segments = 4
	rings = 4
	operation = CSGShape.OPERATION_SUBTRACTION
	material = Mat

func _process(delta):
	if init_radius == null:
		init_radius = radius
	timer = max(timer - delta, 0)
	radius = init_radius * (timer/MAX_TIMER)
	if timer == 0:
		radius = 0.000001
		queue_free()