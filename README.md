# ![Absumoria](https://gitlab.com/meditator/absumoria-godot/raw/master/work/screenshots/cover-a.png)

Made with [Godot Engine](https://godotengine.org). Absumoria [free software](https://www.gnu.org/philosophy/free-sw.en.html). Code is licensed under the [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0-standalone.html). Art and sound is licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

To modify this game, make sure Godot Engine is installed, then open project.godot.
