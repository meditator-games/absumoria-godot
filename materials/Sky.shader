shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_disabled,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform float layer1 = 0.75;
uniform float layer2 = 0.25;

void vertex() {
	UV=UV*uv1_scale.xy+uv1_offset.xy;
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	vec4 albedo_tex_2 = texture(texture_albedo,base_uv*0.64323 + 0.41234);
	float alpha = pow(albedo_tex.r, 4.) * layer1 + pow(albedo_tex_2.r, 2.) * layer2;
	ALBEDO = albedo.rgb;
	ALPHA = albedo.a * alpha;
	EMISSION = ALBEDO;
	TRANSMISSION = ALBEDO;
}
